//
//  NGСustomUserAcceptVC.m
//  test
//
//  Created by Naz on 9/21/15.
//  Copyright © 2015 Naz. All rights reserved.
//

#import "NGCustomUserAcceptVC.h"
#import "AFNetworking.h"
#import "SCLAlertView.h"
#import "UIImageView+AFNetworking.h"
#import "customUserAcceptTableViewCell.h"
#import "NGServerManager.h"
#import "NGWallGroup.h"


@interface NGCustomUserAcceptVC ()<VKSdkDelegate>

@property (strong, nonatomic) NSString *textResponce;

@property (strong, nonatomic) NSString *userID;
@property (nonatomic ,strong) UIRefreshControl *refreshView;
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NSString* tokenVk;
@property (nonatomic, strong) NSMutableArray *postArray;

@end

@implementation NGCustomUserAcceptVC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
     self.postArray = [NSMutableArray array];
    [VKSdk initializeWithDelegate:self andAppId:@"5053942"];
    if ([VKSdk wakeUpSession])
    {
        
        
    }
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self selector:@selector(triggerAction:) name:@"acceptedMissionId" object:nil];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
    self.refreshView = [[UIRefreshControl alloc] init];
    
    [self.refreshView addTarget:self action:@selector(getProducts) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshView];
//    self.okButton.layer.cornerRadius = CGRectGetHeight(self.okButton.bounds)/2;
//    self.okButton.layer.masksToBounds = YES;
//    NSArray *scope = @[@"friends",@"photos",@"audio",@"wall"];
//    
//    
//    [VKSdk authorize:scope revokeAccess:YES forceOAuth:YES];
   [self getProducts];
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller{
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError{
    
    
}
-(void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken{
    
    
}
-(void)vkSdkUserDeniedAccess:(VKError *)authorizationError{
    
    
}
-(void)vkSdkReceivedNewToken:(VKAccessToken *)newToken{
    
    self.tokenVk = newToken.accessToken;
    self.userID = newToken.userId;
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:newToken.accessToken forKey:@"token"];
    [defaults synchronize];
    
    [self getProducts];
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getProducts{
    
    [[NGServerManager sharedManager]
     addowner_id:@"-102872314"
     addalbum_id:@"0"
     addaudio_ids:@"0"
     addneed_user:@"10"
     addoffset:@"all"
     addcount:@""
     addversion:@"5.37"
     
     onSuccess:^(NSArray *dict) {
     
         [self.postArray removeAllObjects];
         [self.postArray addObjectsFromArray:dict];
         
         NSMutableArray* newPaths = [NSMutableArray array];
         for (int i = (int)[self.postArray count] - (int)[dict count]; i < [self.postArray count]; i++) {
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
        [self.refreshView endRefreshing];
        [self.tableView reloadData];
 
     
     
     
     }
     onFailure:^(NSError *error) {
         NSLog(@"");
     }];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.postArray.count + 10;
    //return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 600;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NGWallGroup *group = [NGWallGroup new];


    if (self.tableView == tableView){
        static NSString *simpleTableIdentifier = @"Cell";
        
        customUserAcceptTableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if(!cell){
            cell = [[customUserAcceptTableViewCell alloc] initWithStyle:
                    UITableViewCellStyleDefault      reuseIdentifier:simpleTableIdentifier];
        }
        
    //self.group = self.arrayMessage[indexPath.row];

    // NSInteger TimeStamp = (NSInteger) self.message.time_create;
    
    //NSDate *messageDatea = [NSDate dateWithTimeIntervalSince1970:TimeStamp];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    // NSString *dateString = [dateFormatter stringFromDate:messageDatea];
    
    NSString* photo = [NSString stringWithFormat:@"%@", group.photo_130];
    NSURL *imageURL;
    if (photo) {
        imageURL = [NSURL URLWithString:photo];
    }
    [cell.imageProfile setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"userimg"]];
        cell.nicknameLabel.text = group.text;
    

        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


@end
